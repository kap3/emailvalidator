package email;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	//4 Tests if email contains EXACTLY ONE @ Symbol
	@Test
	public void testEmailContainsOneAtSymbolRegular() {
		boolean result = EmailValidator.containsOnlyOneAtSymbol("11111111@111111.com");
		assertTrue("Must contain exactly one @ symbol", result);
	}
	
	@Test
	public void testEmailContainsOneAtSymbolException() {
		boolean result = EmailValidator.containsOnlyOneAtSymbol("111111.com");
		assertFalse("There is exactly one @ symbol", result);
	}
	
	@Test
	public void testEmailContainsOneAtSymbolBoundaryIn() {
		boolean result = EmailValidator.containsOnlyOneAtSymbol("11111@11111.com");
		assertTrue("Must contain exactly one @ symbol", result);
	}
	
	@Test
	public void testEmailContainsOneAtSymbolBoundaryOut() {
		boolean result = EmailValidator.containsOnlyOneAtSymbol("111@@1111.com");
		assertFalse("There is exactly one @ symbol", result);
	}
	
	//4 Tests to check if account name has 3 characters
	@Test
	public void testEmailAccountNameContainsAtleastThreeCharactersRegular() {
		boolean result = EmailValidator.accountNameContainsThreeCharacters("123456@123.com");
		assertTrue("Email account name must contain atleast 3 characters for the account name!", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAtleastThreeCharactersException() {
		boolean result = EmailValidator.accountNameContainsThreeCharacters("@123.com");
		assertFalse("Email account name contains 3 or more characters already", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAtleastThreeCharactersBoundaryIn() {
		boolean result = EmailValidator.accountNameContainsThreeCharacters("123@123.com");
		assertTrue("Email account name must contain atleast 3 characters for the account name!", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAtleastThreeCharactersBoundaryOut() {
		boolean result = EmailValidator.accountNameContainsThreeCharacters("12@123.com");
		assertFalse("Email account name contains 3 or more characters already", result);
	}
	
	//4 Tests to check if the account name is all lowercase
	@Test
	public void testEmailAccountNameContainsAllLowerCaseCharactersRegular() {
		boolean result = EmailValidator.accountNameContainsOnlyLowerCase("abcdefg@aaaa.com");
		assertTrue("Email account name must contain lowercase letters only!", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAllLowerCaseCharactersException() {
		boolean result = EmailValidator.accountNameContainsOnlyLowerCase("ABCDEFG@aaaa.com");
		assertFalse("Email account name ALREADY contains lowercase letters!", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAllLowerCaseCharactersBoundaryIn() {
		boolean result = EmailValidator.accountNameContainsOnlyLowerCase("a@aaaa.com");
		assertTrue("Email account name must contain lowercase letters only!", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAllLowerCaseCharactersBoundaryOut() {
		boolean result = EmailValidator.accountNameContainsOnlyLowerCase("abcdefG@aaaa.com");
		assertFalse("Email account name ALREADY contains lowercase letters!", result);
	}
	
	
	//4 Tests to check if the account name contains ONLY ALPHA CHARACTERS (doesn't mention alphanumeric characters, assuming alpha characters refers to letters only)
	@Test
	public void testEmailAccountNameContainsAlphaCharactersOnlyRegular() {
		boolean result = EmailValidator.accountNameContainsAlphaCharactersOnly("aaaaaa@123.com");
		assertTrue("Email account name must contain ALPHA CHARACTERS ONLY", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAlphaCharactersOnlyException() {
		boolean result = EmailValidator.accountNameContainsAlphaCharactersOnly("12a_123-@123.com");
		assertFalse("Email account name ALREADY contains alpha characters only", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAlphaCharactersOnlyBoundaryIn() {
		boolean result = EmailValidator.accountNameContainsAlphaCharactersOnly("aAzZ@123.com");
		assertTrue("Email account name must contain ALPHA CHARACTERS ONLY", result);
	}
	
	@Test
	public void testEmailAccountNameContainsAlphaCharactersOnlyBoundaryOut() {
		boolean result = EmailValidator.accountNameContainsAlphaCharactersOnly("abc1@123.com");
		assertFalse("Email account name ALREADY contains alpha characters only", result);
	}
	
	//4 Tests to check if the account name doesn't start with a number
	@Test
	public void testEmailAccountNameDoesNotStartWithNumberRegular() {
		boolean result = EmailValidator.accountNameDoesNotStartWithNumber("aaaaaa@123.com");
		assertTrue("Email account name must NOT begin with a number", result);
	}
	
	@Test
	public void testEmailAccountNameDoesNotStartWithNumberException() {
		boolean result = EmailValidator.accountNameDoesNotStartWithNumber("1aaaaaa@123.com");
		assertFalse("Email account name doesn't start with a number", result);
	}
	
	@Test
	public void testEmailAccountNameDoesNotStartWithNumberBoundaryIn() {
		boolean result = EmailValidator.accountNameDoesNotStartWithNumber("a1@123.com");
		assertTrue("Email account name must NOT begin with a number", result);
	}
	
	@Test
	public void testEmailAccountNameDoesNotStartWithNumberBoundaryOut() {
		boolean result = EmailValidator.accountNameDoesNotStartWithNumber("1a@123.com");
		assertFalse("Email account name doesn't start with a number", result);
	}
	
	//4 Tests to check if the domain name is at least 3 alphanumeric characters (explicitly mentions letters and numbers)
	@Test
	public void testEmailDomainNameContainsAtLeastThreeAlphaNumericRegular() {
		boolean result = EmailValidator.domainNameContainsThreeAlphaNumeric("123@abc123.com");
		assertTrue("Email domain does NOT contain atleast 3 alphanumeric characters", result);
	}
	
	@Test
	public void testEmailDomainNameContainsAtLeastThreeAlphaNumericException() {
		boolean result = EmailValidator.domainNameContainsThreeAlphaNumeric("123@---.com");
		assertFalse("Email domain CONTAINS 3 alphanumeric characters", result);
	}
	
	@Test
	public void testEmailDomainNameContainsAtLeastThreeAlphaNumericBoundaryIn() {
		boolean result = EmailValidator.domainNameContainsThreeAlphaNumeric("123@a1c.com");
		assertTrue("Email domain does NOT contain atleast 3 alphanumeric characters", result);
	}
	
	@Test
	public void testEmailDomainNameContainsAtLeastThreeAlphaNumericBoundaryOut() {
		boolean result = EmailValidator.domainNameContainsThreeAlphaNumeric("123@1-.com");
		assertFalse("Email domain CONTAINS 3 alphanumeric characters", result);
	}
	
	//4 Tests to check if extension is of only letters and atleast 2
	@Test
	public void testEmailExtensionContainsAtLeastTwoAlphaCharactersRegular() {
		boolean result = EmailValidator.extensionContainsTwoAlphaCharacters("123@123.com");
		assertTrue("Email extension does NOT contain atleast 2 alpha characters", result);
	}
	
	@Test
	public void testEmailExtensionContainsAtLeastTwoAlphaCharactersException() {
		boolean result = EmailValidator.extensionContainsTwoAlphaCharacters("123@123.1");
		assertFalse("Email extension contains 2 alpha characters", result);
	}
	
	@Test
	public void testEmailExtensionContainsAtLeastTwoAlphaCharactersBoundaryIn() {
		boolean result = EmailValidator.extensionContainsTwoAlphaCharacters("123@123.ca");
		assertTrue("Email extension does NOT contain atleast 2 alpha characters", result);
	}
	
	@Test
	public void testEmailExtensionContainsAtLeastTwoAlphaCharactersBoundaryOut() {
		boolean result = EmailValidator.extensionContainsTwoAlphaCharacters("123@123.c1");
		assertFalse("Email extension contains 2 alpha characters", result);
	}
	

}
