package email;

/**
 * 
 * @author Seby
 *
 */
public class EmailValidator {

	public static boolean containsOnlyOneAtSymbol(String email) {
		// TODO Auto-generated method stub
		char c = '@';
		int counter = 0;

		for (int i = 0; i < email.length(); i++) {
			if (email.charAt(i) == c) {
				counter++;
			}
		}

		if (counter == 1) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean accountNameContainsThreeCharacters(String email) {
		// TODO Auto-generated method stub
		char c = '@';
		int indexOfAt = email.indexOf(c);
		if (indexOfAt > 2) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean accountNameContainsAlphaCharactersOnly(String email) {
		// TODO Auto-generated method stub
		char c = '@';
		int indexOfAt = email.indexOf(c);
		String lowerEmail = email.toLowerCase();
		for (int i = 0; i < indexOfAt; i++) {
			char ch = lowerEmail.charAt(i);
			if (!(ch >= 'a' && ch <= 'z')) {
				return false;
			}
		}
		return true;
	}

	public static boolean accountNameContainsOnlyLowerCase(String email) {
		// TODO Auto-generated method stub
		char c = '@';
		int indexOfAt = email.indexOf(c);
		for(int i = 0; i< indexOfAt; i++) {
			if (email.charAt(i) >='A' && email.charAt(i) <= 'Z') {
				return false;
			}
		}
		return true;
	}
	
	public static boolean accountNameDoesNotStartWithNumber(String email) {
		// TODO Auto-generated method stub
		boolean result = Character.isDigit(email.charAt(0));
		return !result;
	}

	public static boolean domainNameContainsThreeAlphaNumeric(String email) {
		// TODO Auto-generated method stub
		char c = '@';
		char dot = '.';
		int indexOfAt = email.indexOf(c);
		int indexOfDot = email.indexOf(dot);
		boolean result = false;
		
		String domain = email.substring(indexOfAt+1, indexOfDot);
		if (domain.matches("^[a-zA-Z0-9]*$") && indexOfDot-indexOfAt > 3) {
			result = true;
		}
		
		return result;
	}

	public static boolean extensionContainsTwoAlphaCharacters(String email) {
		// TODO Auto-generated method stub
		//123@123.com
		int counter = 0;
		char dot = '.';
		int indexOfDot = email.indexOf(dot);
		for (int i = indexOfDot+1; i < email.length(); i++) {
			if (!(email.charAt(i) >= 'a' && email.charAt(i) <= 'z')) {
				return false;
			}
			counter++;
		}
		
		return counter >=2;
	}

	

}
